echo on

%% OK
plistxyz(1:3, 1:3, 1:3, 1:3)
plistxyz((1:3)', (1:3)', (1:3)', (1:3)')
plistxyz((1:3)', 1:3, 1:3, 1:3)
plistxyz(1:4, [1 2; 3 4], [1 2; 3 4], [1 2; 3 4])
plistxyz(1:3, 1:3, 1:3, nan)
plistxyz(1:3, nan, nan, 1:3)

plistxyz('ab', 1, 1, 1)
plistxyz(['ab'; 'cd'; 'e '], 1:3, 1:3, 1:3)
plistxyz({'ab','cd','e'}, 1:3, 1:3, 1:3)

%% ERROR
plistxyz(1:3, (1:3)', 1:3, 1:3)
plistxyz(1:3, 1:4, 1:3, 1:3)
plistxyz(1:4, 1:3, 1:3, 1:3)
