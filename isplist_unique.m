function is = isplist_unique(plist)
%%
%% test ids unique
%%

if nargin ~= 1
	usage('isplist_unique(plist)')
end

if ~isplist(plist)
	error('isplist_unique: wrong parametr plist')
end

is = length(plist.id) == length(unique(plist.id));
