
echo on

pl  = plistxyz({'ab','cd','e','f'}, 1:4, 1:4, 1:4)
pl2 = plistxyz([1 2; 3 4], 1:4, 1:4, 1:4)

[x, y, z] = coordxyz(pl,'ab')
[x, y, z] = coordxyz(pl,{'ab','e','f'})
[x, y, z] = coordxyz(pl,{'ab','g','h'})

[x, y, z] = coordxyz(pl2,1:3)
[x, y, z] = coordxyz(pl2,1:6)
