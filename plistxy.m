function plist = plistxy(id, x, y)
%%
%% create point list
%%

if nargin ~= 3
	usage('plistxy(id, x, y)')
end

plist = plistxyz(id, x, y, nan);
