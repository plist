function [x, y, z] = coordxyz(plist, id)
%%
%% finds coordinates of points by its id
%%

if nargin < 1 | nargin > 2
	usage('coordxyz(plist, [id])')
end

if ~isplist(plist)
	warning('coordxyz: wrong type of parameter plist')
	x = nan; y = nan; z = nan;
	return
end


if ~isplist_id(id)
	warning('coordxyz: wrong type of parameter id')
	x = nan; y = nan; z = nan;
	return
end

if nargin == 1
	x = plist.coordxyz(:,1);
	y = plist.coordxyz(:,2);
	z = plist.coordxyz(:,3);
	return
end

[id idsize] = plistid2cell(id);

x_ = plist.coordxyz(:,1)';
y_ = plist.coordxyz(:,2)';
z_ = plist.coordxyz(:,3)';

for i=1:prod(idsize)
	fh = @(x) strcmp(x, id{i});
	cmp = cellfun(fh, plist.id);
	if ~any(cmp)
		warning('coordxyz: point id="%s" is not in point list', id{i})
		x(i) = nan; y(i) = nan; z(i) = nan;
	else
		x(i) = x_(cmp);
		y(i) = y_(cmp);
		z(i) = z_(cmp);
	end
end
