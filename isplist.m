function is = isplist(plist)
%% function is = isplist(plist)
%% tests if the argument is point list

is = isfield(plist,'id') & isfield(plist,'coordxyz');
if is
	is = length(plist.id) == size(plist.coordxyz,1) & ...
	     size(plist.coordxyz,2) == 3;
end
