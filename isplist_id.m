function is = isplist_id(id)
%%
%% function is = isplist_id(id)
%%
%% tests if id is id of point

if ( nargin < 1 )
	usage('isplist_id(id)')
end

is = iscell(id) | ischar(id) | isnumeric(id);

