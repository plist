function [x,y] = coordxy(plist, id)
%% function [x,y] = coordxy(plist, id)
%% returns x and y coordinates of point specified by id

if ( nargin~= 2 )
	usage('[x,y] = coordxy(plist, id)')
end

%% vyhledani bodu
point_id = point(plist, id);

%% testovani zda jsou vsechny body typu 2 nebo 3
type = all(istype(point_id, 2) | istype(point_id, 3));
if ( ~type )
	ptype0 = pointtype(point_id, 0);
	ptype1 = pointtype(point_id, 1);
	pprint(ptype0, ptype1);
end

x = reshape([point_id.x], size(id));
y = reshape([point_id.y], size(id));

if ( nargout == 1 )
	x = [x y];
end
