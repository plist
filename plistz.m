function plist = plistz(id, z)
%%
%% create point list
%%

if nargin ~= 2
	usage('plistz(id, z)')
end

plist = plistxyz(id, nan, nan, z);
