function d = plist_distance(plist, id1, id2)
%% function d = distance(pl, id1, id2)
%% computes distance between two points id1 and id2
%% the distance is horizonthal -- x,y coodrinates are used

if ( nargin != 3 )
	usage('d = plist_distance(plist, id1, id2)')
end

if ( !isplist(plist) )
	error('plist_distance: The first parameter is not plist')
end

[tst, id1, id2] = common_id(id1, id2);
if ( tst )
	error('plist_distance: Argument id1 or id2 are not ids of points.')
end

[x1, y1] = coordxy(plist, id1);
[x2, y2] = coordxy(plist, id2);
d = sqrt((x2-x1).^2 + (y2-y1).^2);
