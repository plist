function plist = plistxyz(id, x, y, z)
%% create point list

if nargin ~= 4
	usage('plistxyz(id, x, y, z)')
end

if ~isplist_id(id)
	warning('plistxyz: wrong type of parameter id')
	plist = nan;
	return
end

[tst, x, y, z] = common_size(x, y, z);
if tst
	warning('plistxyz: wrong sizes of x, y, z')
	plist = nan;
	return
end

id = plistid2cell(id);

if numel(x) ~= numel(id)
	warning('plistxyz: the size of id do not equal to size of x, y and z')
	plist = nan;
	return
end

plist.id = id;
plist.coordxyz = cat(2,x(:),y(:),z(:));

if ~isplist_unique(plist)
	warning('plistxyz: ids are not unique')
end

