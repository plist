#!/usr/bin/octave --silent

echo on

[tst, a, b] = common_id(4001, 1)
[tst, a, b] = common_id(4001, [1 2; 3 4])
[tst, a, b] = common_id([4001 4002], [1 2])
[tst, a, b] = common_id('4001', '1')
[tst, a, b] = common_id('4001', {'1' '2'; '3' '4'})
[tst, a, b] = common_id({'4001' '4002'}, {'1' '2'})
[tst, a, b] = common_id({'4001' '4002'}, [1 2])
[tst, a, b] = common_id([4001 4002], {'1' '2'})
[tst, a, b, c, d] = common_id([4001 4002], {'1' '2'}, '4001', [1 2])
