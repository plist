function [idc, idsize] = plistid2cell(id)
%%
%% convert points id to cell array
%% creates a row vector of string cells
%%

if nargin ~= 1
	usage('[idc, idsize] = plistid2cell(id)')
end

if isnumeric(id)
	idsize = size(id);
	for i=1:prod(idsize)
		idc{i} = num2str(id(i));
	end
	% idc = reshape(idc, idsize);
elseif ischar(id)
	for i=1:size(id, 1)
		idc{i} = deblank(id(i,:));
	end
	idsize = [size(id, 1) 1];
else
	idc = id;
	idsize = size(id);
end
