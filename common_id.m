function [tst, varargout] = common_id(varargin)
%% function common_id tests if ids of points 
%% are of the same size and propper type

if ( nargin < 2 )
	usage('common_id(id1, id2, ...)')
end

pid = all(cellfun(@isplist_id, varargin));
if ( ~pid )
	tst = 1;
	varargout = varargin;
	return
end

tst = false;
id_size = 1;

%% posouzeni rozmeru poli krome pole typu char a poli o jednom prvku
for i=1:nargin
	id = varargin{i};
	if ( length(id) != 1 & ~ischar(id) )
		if ( id_size ~= 1) 
			tst = tst | any(id_size!=size(id));
		end
		id_size = size(id);
	end
end

%% skalarni expanze poli
for i=1:nargin
	id = varargin{i};
	if ( length(id) == 1 & ~ischar(id) )
		args{i} = repmat(id, id_size);
	elseif ( ischar(id) )
		args{i} = repmat({id}, id_size);
	else
		args{i} = id;
	end
end

varargout = args;
