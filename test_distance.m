#!/usr/bin/octave --silent

echo on


plist = struct('id', {'4001' '4002' '1' '2'},...
	       'x',  {0 1000 100 500},...
	       'y',  {0 1000 100 500},...
	       'type', {2 2 2 2});
plist.id
plist(2)
distance(plist, 4001, 1)
distance(plist, 4001, [1 2; 3 4])
distance(plist, [4001 4002], [1 2])
distance(plist, '4001', '1')
distance(plist, '4001', {'1' '2'; '3' '4'})
distance(plist, {'4001' '4002'}, {'1' '2'})
distance(plist, {'4001' '4002'}, [1 2])
distance(plist, [4001 4002], {'1' '2'})
